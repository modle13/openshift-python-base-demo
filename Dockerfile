FROM python:3.10

ENV VIRTUALENV_PATH=/opt/venv

WORKDIR /app
COPY . /app

RUN python -m venv $VIRTUALENV_PATH

# enable env
ENV PATH="$VIRTUALENV_PATH/bin:$PATH"

# Upgrade and install pip, setuptools and gunicorn
# RUN pip install --upgrade --no-cache-dir pip setuptools

RUN pip install -U --no-cache-dir -r requirements/requirements.txt

EXPOSE 5000

ENV PATH="$VIRTUALENV_PATH/bin:$PATH"
CMD ["python3", "helloworld.py"]
